<?php

class Mpress
{
	protected $loader;
	protected $plugin_slug;
	protected $version;

	public function __construct()
	{
		$this->plugin_slug = 'mpress-slug';
		$this->version = '0.1.0';

		$this->load_dependencies();
		$this->define_admin_hooks();
	}

	private function load_dependencies()
	{
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-mpress-admin.php';

		require_once plugin_dir_path( __FILE__ ) . 'class-mpress-loader.php';
		$this->loader = new Mpress_Loader();
	}

	private function define_admin_hooks()
	{
		$admin = new Mpress_Admin( $this->get_version() );
		
		//Adding Admin Styles
		$this->loader->add_action( 'admin_enqueue_scripts', $admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $admin, 'enqueue_scripts' );
		
		//Register Settings
		$this->loader->add_action( 'admin_init', $admin, 'mpress_register_settings' );

		//Adding Menus
		$this->loader->add_action( 'admin_menu', $admin, 'register_admin_menu_dashboard' );
		
		//Adding Custom Post Types:
		//Adding Custom Post Type: Menus, Pages
		//Source: http://bit.ly/1ZkBSXr
		$this->loader->add_action( 'init', $admin, 'register_posttype_mpressmenus' );
		$this->loader->add_action( 'admin_menu', $admin, 'register_admin_menu_mpressmenus' );
		$this->loader->add_action( 'add_meta_boxes', $admin, 'add_meta_box' );
		
		$this->loader->add_action( 'init', $admin, 'register_posttype_mpresspages' );
		$this->loader->add_action( 'admin_menu', $admin, 'register_admin_menu_mpresspages' );

		//Saving options of mpressmenus post type when a post is updated
		$this->loader->add_action( 'save_post', $admin, 'update_menulink' );

		//Saving a post in admin area using ajax
		$this->loader->add_action( 'wp_ajax_mpress_save_options', $admin, 'process_mpress_options_form' );

		$this->loader->add_action( 'admin_init', $admin, 'output_test' );

	}

	public function run()
	{
		$this->loader->run();
	}

	public function get_version()
	{
		return $this->version;
	}

}
