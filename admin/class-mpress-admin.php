<?php

class Mpress_Admin
{

	private $version;
	private $admin_page_address;
	private $plugin_output_address;

	public function __construct( $version )
	{
		$this->version = $version;
		$this->admin_page_address = "admin.php?page=mpress";
		$this->plugin_output_address = plugin_dir_path(dirname(__FILE__)) . 'outputs/';
	}

	public function enqueue_styles()
	{
		wp_enqueue_style(
			'mpress-admin',
			plugin_dir_url( __FILE__ ) . 'css/mpress-admin.css',
			array(),
			$this->version,
			FALSE
		);
	}

	public function enqueue_scripts()
	{
		wp_enqueue_script(
			'mpress-admin',
			plugin_dir_url( __FILE__ ) . 'js/mpress-admin.js',
			array(),
			$this->version,
			FALSE
		);
	}

	public function register_admin_menu_dashboard()
	{
		add_menu_page(
			'Mpress Admin Area',
			'MPress',
			'manage_options',
			'mpress',
			array( $this, 'admin_menu_dashboard_content' ),
			plugin_dir_url( __FILE__ ) . 'images/mpress.png',
			6 );
	}

	public function register_admin_menu_menus()
	{
		add_submenu_page(
			'mpress',
			'MPress Menus',
			'Menus',
			'manage_options',
			'mpress-menus',
			array( $this, 'admin_menu_menus_content' )
		);
	}

	public function register_posttype_mpressmenus()
	{
		$labels = array(
			'name'               => _x( 'Menus', 'post type general name', 'your-plugin-textdomain' ),
			'singular_name'      => _x( 'Menu', 'post type singular name', 'your-plugin-textdomain' ),
			'menu_name'          => _x( 'Menus', 'admin menu', 'your-plugin-textdomain' ),
			'name_admin_bar'     => _x( 'Menu', 'add new on admin bar', 'your-plugin-textdomain' ),
			'add_new'            => _x( 'Add New', 'menu', 'your-plugin-textdomain' ),
			'add_new_item'       => __( 'Add New Menu', 'your-plugin-textdomain' ),
			'new_item'           => __( 'New Menu', 'your-plugin-textdomain' ),
			'edit_item'          => __( 'Edit Menu', 'your-plugin-textdomain' ),
			'view_item'          => __( 'View Menu', 'your-plugin-textdomain' ),
			'all_items'          => __( 'All Menus', 'your-plugin-textdomain' ),
			'search_items'       => __( 'Search Menus', 'your-plugin-textdomain' ),
			'parent_item_colon'  => __( 'Parent Menus:', 'your-plugin-textdomain' ),
			'not_found'          => __( 'No Menus found.', 'your-plugin-textdomain' ),
			'not_found_in_trash' => __( 'No Menus found in Trash.', 'your-plugin-textdomain' )
		);

		$args = array(
			'labels'             => $labels,
	        //'description'        => __( 'Description.', 'your-plugin-textdomain' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => false,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'mpressmenus' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title' )
		);

		register_post_type( 'mpressmenus', $args );
	}

	public function register_posttype_mpresspages()
	{
		$labels = array(
			'name'               => _x( 'Pages', 'post type general name', 'your-plugin-textdomain' ),
			'singular_name'      => _x( 'Page', 'post type singular name', 'your-plugin-textdomain' ),
			'menu_name'          => _x( 'Pages', 'admin menu', 'your-plugin-textdomain' ),
			'name_admin_bar'     => _x( 'Page', 'add new on admin bar', 'your-plugin-textdomain' ),
			'add_new'            => _x( 'Add New', 'menu', 'your-plugin-textdomain' ),
			'add_new_item'       => __( 'Add New Page', 'your-plugin-textdomain' ),
			'new_item'           => __( 'New Page', 'your-plugin-textdomain' ),
			'edit_item'          => __( 'Edit Page', 'your-plugin-textdomain' ),
			'view_item'          => __( 'View Page', 'your-plugin-textdomain' ),
			'all_items'          => __( 'All Pages', 'your-plugin-textdomain' ),
			'search_items'       => __( 'Search Pages', 'your-plugin-textdomain' ),
			'parent_item_colon'  => __( 'Parent Pages:', 'your-plugin-textdomain' ),
			'not_found'          => __( 'No Pages found.', 'your-plugin-textdomain' ),
			'not_found_in_trash' => __( 'No Pages found in Trash.', 'your-plugin-textdomain' )
		);

		$args = array(
			'labels'             => $labels,
	        //'description'        => __( 'Description.', 'your-plugin-textdomain' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => false,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'mpresspages' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'comments' )
		);

		register_post_type( 'mpresspages', $args );
	}

	public function register_admin_menu_mpressmenus()
	{
		add_submenu_page(
			'mpress',
			'MPress Menus',
			'Menus',
			'manage_options',
			'edit.php?post_type=mpressmenus',
			NULL );
	}

	public function register_admin_menu_mpresspages()
	{
		add_submenu_page(
			'mpress',
			'MPress Menus',
			'Pages',
			'manage_options',
			'edit.php?post_type=mpresspages',
			NULL );
	}

	public function add_meta_box()
	{
		add_meta_box(
			'mpress-admin',
			'Mpress MetaBox',
			/* $this->render_meta_box() dosent work! why? Even 'render_meta_box' */
			array( $this, 'render_meta_box' ),
			'mpressmenus',
			'normal',
			'core'
		);
	}

	public function mpress_register_settings()
	{
		
	}

	public function update_menulink($id)
	{
		if(isset($_POST['mpress_menu_link']))
		{
			update_option('mpress_menu_link_' . $id , $_POST['mpress_menu_link'] );
		}
	}

	public function process_mpress_options_form()
	{
		if(isset($_POST['mpress_new_menu_title']) && !empty($_POST['mpress_new_menu_title']))
		{
			$insert_menu = array(
			  'post_title'   	=> sanitize_text_field( $_POST['mpress_new_menu_title'] ),
			  'post_status'  	=> 'publish',
			  'post_type'  		=> 'mpressmenus'
			);
			// Insert the post into the database
			wp_insert_post( $insert_menu );

			echo "Form Submitted Successfully via ajax! means the new menu Created";

			//use this in non ajax form submition!:
			//wp_redirect( admin_url( $this->admin_page_address . '&create_menu_message=1' ) );
   			die();
		}
	}

	public function output_test()
	{
		$args = array(
		  'post_type' => 'mpresspages',
		  'post_status' => 'publish',
		  'posts_per_page' => -1, // all
		  'orderby' => 'title',
		  'order' => 'ASC'
		);
		 
		$query = new WP_Query( $args );
		 
		$mpresspages = array();
		 
		while( $query->have_posts() ) : $query->the_post();
		 
		  // Add a car entry
		  $mpresspages[] = array(
		    'title' => get_the_title(),
		    'content' => get_the_content()
		  );
		 
		endwhile;
		wp_reset_query();

		$mpresspagesJSON = json_encode($mpresspages);

		try
		{
			$Handle = fopen( $this->plugin_output_address . 'pages.json' , 'w');
			fwrite($Handle, $mpresspagesJSON);
			fclose($Handle);
		}
		catch (Exception $e)
		{
			die('JSON Creation Error: ' . $e->getMessage());
		}

	}

	/*
	Partial Functions
	*/
	public function render_meta_box()
	{
		require_once plugin_dir_path( __FILE__ ) . 'partials/partial-mpress.php';
	}

	public function admin_menu_dashboard_content()
	{
		require_once plugin_dir_path( __FILE__ ) . 'partials/partial-mpress-admin-menu-dashboard.php';
	}

	public function admin_menu_menus_content()
	{
		require_once plugin_dir_path( __FILE__ ) . 'partials/partial-mpress-admin-menu-menus.php';
	}
}
