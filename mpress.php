<?php
/**
 * @package mpress
 */
/*
Plugin Name:		MPress
Plugin URI:			mpress.ir
Description:		Controlling Mobile Application of your site with Wordpress
Version:			0.1.0
Author:				Mostafa Qanbari
Author URI: 
Text Domain:		mpress
License:			GPLv2 or later
Text Domain:		mpress
Domain Path:       /languages
*/

if ( !defined('WPINC') )
{
	die;
}

require_once plugin_dir_path( __FILE__ ) . 'includes/class-mpress.php';

$mpress = new Mpress();
$mpress->run();

